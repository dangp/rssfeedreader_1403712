package com.example.haiphan.rssfeedreader;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.example.haiphan.rssfeedreader.model.FeedItem;
import com.example.haiphan.rssfeedreader.model.User;

/**
 * Created by HaiPhan on 10/10/2015.
 */
public class FeedItemViewActivity extends AppCompatActivity {
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_item_content);

        Bundle bundle = this.getIntent().getExtras();
        String feedItemContent = bundle.getString("link");

        webView = (WebView) findViewById(R.id.feedItemContent);
        webView.setWebViewClient(new MyWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(feedItemContent);               //load given URL in WebView
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.webview_actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.fav:
                Bundle bundle = this.getIntent().getExtras();
                int feedItemPosition = bundle.getInt("position");
                FeedItem feedItem = User.getInstance().getListFeed().get(feedItemPosition);
                if (!User.getInstance().getListFavorite().contains(feedItem)) {    //check if listFeedItem has feedItem or not
                    User.getInstance().getListFavorite().add(feedItem);               //add feedItem to the favorite list
                    Toast.makeText(getApplicationContext(), "Added to favorites", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "This article has already been added", Toast.LENGTH_SHORT).show();
                }
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
