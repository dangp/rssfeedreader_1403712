package com.example.haiphan.rssfeedreader.async_task;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.example.haiphan.rssfeedreader.R;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by HaiPhan on 10/11/2015.
 */
public class ThumbnailDownload extends AsyncTask<String, Void, Bitmap> {
    private final WeakReference<ImageView> imageViewReference;

    public ThumbnailDownload(ImageView imageView) {
        imageViewReference = new WeakReference(imageView);
    }

    private Bitmap downloadBitmap(String url) {
        Bitmap bmp = null;
        InputStream is = null;
        try {
            URL ulrn = new URL(url);
            HttpURLConnection con = (HttpURLConnection) ulrn.openConnection();
            is = con.getInputStream();
            bmp = BitmapFactory.decodeStream(is);   //download image

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return bmp;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        return downloadBitmap(params[0]);
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if (isCancelled()) {
            result = null;
        }
        if (imageViewReference != null) {
            ImageView imageView = imageViewReference.get();
            if (imageView != null) {

                if (result != null) {
                    imageView.setImageBitmap(result);       //set thumbnail
                } else {
                    imageView.setImageDrawable(imageView.getContext().getResources().getDrawable(R.drawable.rss)); //set default thumbnail
                }
            }

        }
    }
}
