package com.example.haiphan.rssfeedreader;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.haiphan.rssfeedreader.adapter.FeedAdapter;
import com.example.haiphan.rssfeedreader.model.FeedItem;
import com.example.haiphan.rssfeedreader.model.User;
import com.example.haiphan.rssfeedreader.xml_parser.FeedReader;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements Observer, NavigationDrawerFragment.NavigationDrawerCallbacks {
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private ListView listView;
    private ArrayList<FeedItem> feedToShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set up the navigation drawer
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

        User.getInstance().addObserver(this);

        String feedURL = "http://rss.cnn.com/rss/cnn_topstories.rss";
        readFeed(feedURL);

        listView = (ListView) findViewById(R.id.listFeedItem);
        listView.setOnItemClickListener(onItemClickListener);
    }

    //handle when users click on an article
    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            FeedItem feed = User.getInstance().getListFeed().get(position);
            Bundle feedItemInfo = new Bundle();
            feedItemInfo.putString("link", feed.getFeedItemLink());
            feedItemInfo.putInt("position", position);

            Intent feedItemViewIntent = new Intent(MainActivity.this, FeedItemViewActivity.class);
            feedItemViewIntent.putExtras(feedItemInfo);
            startActivity(feedItemViewIntent);

        }
    };

    @Override //handle when users click on an item of navigation drawer
    public void onNavigationDrawerItemSelected(int position) {
        switch (position) {
            case 0:
                feedToShow = User.getInstance().getListFeed();
                update(feedToShow);
                break;
            case 1:
                feedToShow = User.getInstance().getListFavorite();
                update(feedToShow);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
    }

    @Override //update new feed items on main screen
    public void update(final ArrayList<FeedItem> listFeedUpdated) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    listView = (ListView) findViewById(R.id.listFeedItem);
                    FeedAdapter feedAdapter = new FeedAdapter(MainActivity.this, R.layout.feed_item, listFeedUpdated);
                    listView.setAdapter(feedAdapter);
                    feedAdapter.notifyDataSetChanged();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.action_bar, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.add:
                addFeed();
                return true;
            case R.id.sortByTitle:
                sortByTitle();
                return true;
            case R.id.sortByDate:
                sortByDate();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sortByDate() {
        Collections.sort(feedToShow, new FeedItem());
        update(feedToShow);
    }

    private void sortByTitle() {
        Collections.sort(feedToShow, FeedItem.FeedTitleComparator);
        update(feedToShow);
    }

    public void addFeed() {
        LayoutInflater factory = LayoutInflater.from(this);
        final View view = factory.inflate(R.layout.add_feed_dialog, null);
        final AlertDialog d = new AlertDialog.Builder(this).create();
        d.setView(view);
        d.setTitle("Add Feed");
        d.setCancelable(true);

        Button button = (Button) view.findViewById(R.id.addFeed);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText website = (EditText) view.findViewById(R.id.editText);      //get the name of website from users
                EditText editText = (EditText) view.findViewById(R.id.feedURLInput); //get the RSS link input from users
                String urlInput = editText.getText().toString();
                if (!urlInput.isEmpty() && URLUtil.isHttpUrl(urlInput) && !website.getText().toString().isEmpty()) {
                    if (!User.getInstance().getListRSSUrl().contains(urlInput)) {     //check if the link has been added before
                        readFeed(urlInput);
                        User.getInstance().getListWebsite().add(website.getText().toString());
                        Toast.makeText(getApplicationContext(), "Feed added", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "This feed has already been added", Toast.LENGTH_LONG).show();
                    }
                    d.dismiss();
                }
            }
        });
        d.show();
    }

    public void readFeed(String url) {
        if (!User.getInstance().getListRSSUrl().contains(url)) {            //check if this XML file has been read
            FeedReader feedReader = new FeedReader(url);
            Thread t = new Thread(feedReader);
            t.start();
            User.getInstance().getListRSSUrl().add(url);                    //add link to the list of XML links
        }
    }
}
