package com.example.haiphan.rssfeedreader.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

/**
 * Created by HaiPhan on 10/6/2015.
 */
public class FeedItem implements Comparator<FeedItem> {
    private String feedItemThumbURL;
    private String feedItemTitle;
    private String feedItemDate;
    private String feedItemLink;

    // get value methods
    public String getFeedItemThumbURL() {
        return this.feedItemThumbURL;
    }

    public String getFeedItemTitle() {
        return this.feedItemTitle;
    }

    public String getFeedItemDate() {
        return this.feedItemDate;
    }

    public String getFeedItemLink() {
        return this.feedItemLink;
    }

    // set value methods
    public void setFeedItemThumbURL(String thumbURL) {
        this.feedItemThumbURL = thumbURL;
    }

    public void setFeedItemTitle(String title) {
        this.feedItemTitle = title;
    }

    public void setFeedItemDate(String date) {
        this.feedItemDate = date;
    }

    public void setFeedItemLink(String link) {
        this.feedItemLink = link;
    }

    //sort methods
    @Override //compare pubDate of feed items
    public int compare(FeedItem feedItem1, FeedItem feedItem2) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, DD MMM yyyy HH:mm:ss", Locale.US);
        int result = 0;
        try {
            Date date1 = dateFormat.parse(feedItem1.getFeedItemDate());
            Date date2 = dateFormat.parse(feedItem2.getFeedItemDate());
            result = date2.compareTo(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    // compare title of feed items
    public static Comparator<FeedItem> FeedTitleComparator = new Comparator<FeedItem>() {
        @Override
        public int compare(FeedItem feedItem1, FeedItem feedItem2) {
            String feedTitle1 = feedItem1.getFeedItemTitle().toUpperCase();
            String feedTitle2 = feedItem2.getFeedItemTitle().toUpperCase();
            return feedTitle1.compareTo(feedTitle2);
        }

    };
}
