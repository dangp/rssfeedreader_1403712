package com.example.haiphan.rssfeedreader;

import com.example.haiphan.rssfeedreader.model.FeedItem;

import java.util.ArrayList;

/**
 * Created by HaiPhan on 10/10/2015.
 */
public interface Observer {
    void update(ArrayList<FeedItem> listFeedUpdated);
}
