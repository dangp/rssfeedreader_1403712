package com.example.haiphan.rssfeedreader.xml_parser;

import android.util.Log;

import com.example.haiphan.rssfeedreader.model.FeedItem;
import com.example.haiphan.rssfeedreader.model.User;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by HaiPhan on 10/10/2015.
 */
public class FeedReader implements Runnable {
    private String feedUrl;

    public FeedReader(String feedURL) {
        this.feedUrl = feedURL;
    }

    @Override
    public void run() {
        try {
            //get connection
            URL url = new URL(feedUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            InputStream stream = conn.getInputStream();
            //parse data
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(stream, null);

            FeedItem feedItem = null;
            boolean insideItem = false; //check if inside tag <item> or not
            String text = null;
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, DD MMM yyyy HH:mm:ss", Locale.US);

            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagName = xpp.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagName.equalsIgnoreCase("item")) {
                            insideItem = true;
                            feedItem = new FeedItem();
                        }
                        break;
                    case XmlPullParser.TEXT:
                        text = xpp.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (tagName.equalsIgnoreCase("item")) {
                            insideItem = false;
                            Date pubDate = dateFormat.parse(feedItem.getFeedItemDate());
                            feedItem.setFeedItemDate(dateFormat.format(pubDate));
                            //add new item to the list and notify to all observers
                            User.getInstance().getListFeed().add(feedItem);
                            User.getInstance().notifyObservers(User.getInstance().getListFeed());
                        } else if (tagName.equalsIgnoreCase("title")) {
                            if (insideItem) {
                                feedItem.setFeedItemTitle(text);
                            }
                        } else if (tagName.equalsIgnoreCase("thumbnail")) {
                            if (insideItem) {
                                feedItem.setFeedItemThumbURL(xpp.getAttributeValue(null, "url"));
                            }
                        } else if (tagName.equalsIgnoreCase("pubDate")) {
                            if (insideItem) {
                                feedItem.setFeedItemDate(text);
                            }
                        } else if (tagName.equalsIgnoreCase("link")) {
                            if (insideItem) {
                                feedItem.setFeedItemLink(text);
                            }
                        }
                        break;
                    default:
                        break;
                }
                eventType = xpp.next();
            }
            stream.close();
            Log.v("Number of feed items: ", String.valueOf((User.getInstance().getListFeed().size())));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
