package com.example.haiphan.rssfeedreader.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.haiphan.rssfeedreader.R;
import com.example.haiphan.rssfeedreader.async_task.ThumbnailDownload;
import com.example.haiphan.rssfeedreader.model.FeedItem;

import java.util.ArrayList;

/**
 * Created by HaiPhan on 10/6/2015.
 */
public class FeedAdapter extends ArrayAdapter<FeedItem> {
    private Activity context;
    private LayoutInflater inflater;
    private ArrayList<FeedItem> feed;

    public FeedAdapter(Context context, int textViewResourceId, ArrayList objects) {
        super(context, textViewResourceId, objects);
        this.context = (Activity) context;
        this.feed = objects;
    }

    static class ViewHolder {
        TextView feedTitleView;
        TextView feedDateView;
        ImageView feedThumbURLView;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.feed_item, null);
            viewHolder = new ViewHolder();
            viewHolder.feedThumbURLView = (ImageView) convertView.findViewById(R.id.feedThumb);
            viewHolder.feedTitleView = (TextView) convertView.findViewById(R.id.feedTitle);
            viewHolder.feedDateView = (TextView) convertView.findViewById(R.id.feedDate);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (viewHolder.feedThumbURLView != null) {
            new ThumbnailDownload(viewHolder.feedThumbURLView).execute(feed.get(position).getFeedItemThumbURL());  //set thumbnail
        }
        viewHolder.feedTitleView.setText(feed.get(position).getFeedItemTitle());
        viewHolder.feedDateView.setText(feed.get(position).getFeedItemDate());

        return convertView;
    }


}
