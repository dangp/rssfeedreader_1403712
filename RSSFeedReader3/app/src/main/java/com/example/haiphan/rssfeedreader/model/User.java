package com.example.haiphan.rssfeedreader.model;

import com.example.haiphan.rssfeedreader.Observer;

import java.util.ArrayList;

/**
 * Created by HaiPhan on 10/10/2015.
 */
public class User {
    private ArrayList<Observer> listObserver;           //list of Observers
    private ArrayList<FeedItem> listFeed;              //list of feed items
    private ArrayList<String> listRSSUrl;               //list of RSS links
    private ArrayList<String> listWebsite;
    private ArrayList<FeedItem> listFavorite;           //list of favorite items
    private static final User INSTANCE = new User();

    private User() {
        listObserver = new ArrayList<>();
        listFeed = new ArrayList<>();
        listRSSUrl = new ArrayList<>();
        listWebsite = new ArrayList<>();
        listFavorite = new ArrayList<>();
    }

    public static User getInstance() {
        return INSTANCE;
    }

    public void addObserver(Observer e) {
        this.listObserver.add(e);
    }

    public void notifyObservers(ArrayList<FeedItem> listFeedUpdated) {
        for (Observer e : listObserver) {
            e.update(listFeedUpdated);
        }
    }

    public ArrayList<FeedItem> getListFeed() {
        return this.listFeed;
    }

    public ArrayList<String> getListRSSUrl() {
        return this.listRSSUrl;
    }

    public ArrayList<String> getListWebsite() {
        return this.listWebsite;
    }

    public ArrayList<FeedItem> getListFavorite() {
        return this.listFavorite;
    }
}
